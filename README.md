# ID Validate

## Sypnosis

ID Validate is a solution with which the identity of a person can be validated through facial, voice or finger recognition.

Its objective is to reduce fraud by impersonation by confirming the identity of the user who requests a financial service from an entity, comparing the information with multiple factors.

## [Pitch](https://www.canva.com/design/DAElEJrCuac/CgACZk8g5UvsFeYc4lGYIQ/view?utm_content=DAElEJrCuac&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink) 📖


## Technologies and tools

* Facial recognition: 
It is done through software, in which the image of the user's face is taken and compared with the database in real time.
It uses Python libraries such as Opencv, os, imutils, numpy, among others.

* Fingerprint recognition:
It is done through software or with national databases collation.
To do this through the software we will use Python and libraries such as Opencv, numpy, matploitlib, enhance, skimage, among others.

* Speech recognition:
It will be done through software, in this case the user is asked to read a sentence, where we capture their voice and compare it, we use Python libraries such as: speech_recognition, among others.



![JavaScript](https://img.shields.io/badge/-JavaScript-black?logo=javascript&style=social)

![Python](https://img.shields.io/badge/-Python-black?logo=Python&style=social)

![HTML5](https://img.shields.io/badge/-HTML5-black?logo=html5&style=social)

![CSS3](https://img.shields.io/badge/-CSS3-black?logo=css3&style=social)

![BitBucket](https://img.shields.io/badge/-Bitbucket-black?logo=bitbucket&style=social)

![Jira](https://img.shields.io/badge/-Jira-black?logo=jirasoftware&style=social)


## Authors ✒️

Nataly Mora

Diana Gaona

Leidy J. saldaña

Yuri Moreno

## License

Public Domain. No copy write protection.


## Gratitude

[Zibone](https://www.zinobe.com/)

And all mentors:

Jorge Hernández

Fernando Yepes