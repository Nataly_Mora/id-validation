from flask import Flask, render_template, Response
import cv2
from faceDetector import Video

app= Flask (__name__)
camera = cv2.VideoCapture();

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/detectorFace')

def video():
    return Response(Video())

if __name__== "__main__":
    app.run(debug=False)